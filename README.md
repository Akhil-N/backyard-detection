# Backyard Detection
 ## Func - backYardFns/split_front_back
 - Function to split aois & Foliage(optional)
 - Refer func to check the inputs & outputs
 ## Func - splitLawnByBackForest
 - Function to clip the lawn by back forest foliage, if complete forest cover present in backyard parcel
 