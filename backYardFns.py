from os.path import *
import pandas as pd, geopandas as gpd, numpy as np,shapely,math

from shapely.geometry import LineString,Point

# corner_property_flag =0 : Standard property
# corner_property_flag =1 : Corner property
# corner_property_flag =2 : Two-Side Road property
# corner_property_flag =3 : Error property

def angle_between_lines_new(first_segment, last_segment):
    if first_segment.coords == last_segment.coords:
        # print("---- Same segments detected, returning angel = 0 !!!")
        return 0
    try:
        m1 = float(
            first_segment.coords.xy[1][1] - first_segment.coords.xy[1][0]
        ) / float(first_segment.coords.xy[0][1] - first_segment.coords.xy[0][0])
    except ZeroDivisionError:
        m1=-1
    try:
        m2 = float(last_segment.coords.xy[1][1] - last_segment.coords.xy[1][0]) / float(
            last_segment.coords.xy[0][1] - last_segment.coords.xy[0][0]
        )
    except ZeroDivisionError:
        m2 = -1
    if m1==m2==-1:
        return 0
    elif m1==-1 or m2==-1:
        m1 = 0 if m1==-1 else m1
        m2 = 0 if m2==-1 else m2
        theta = 90 - math.degrees(math.atan((m1 - m2) / (1 + m1 * m2)))
    else : theta = math.degrees(math.atan((m1 - m2) / (1 + m1 * m2)))
    return theta

def extract_poly_coords(geom):
    if geom.type == 'Polygon':
        exterior_coords = geom.exterior.coords[:]
        interior_coords = []
        for interior in geom.interiors:
            interior_coords.append(interior.coords[:])
    elif geom.type == 'MultiPolygon':
        exterior_coords = []
        interior_coords = []
        for part in geom:
            epc = extract_poly_coords(part)  # Recursive call
            exterior_coords.extend(epc['exterior_coords'])
            interior_coords.append(epc['interior_coords'])
    else:
        raise ValueError('Unhandled geometry type: ' + repr(geom.type))
    return {'exterior_coords': exterior_coords,
            'interior_coords': interior_coords}

def getFlattedListCoords(coord_list):
    if coord_list:
        if isinstance(coord_list,list) and isinstance(coord_list[0],list):
            tmpLst = []
            for t in coord_list:
                out = getFlattedListCoords(t)
                if out:
                    if isinstance(out,list) and isinstance(out[0],list):
                        tmpLst.extend(out)
                    else:
                        tmpLst.append(out)

            return tmpLst
        else:
            return coord_list
    else:
        return coord_list

def get_m_c_of_line(pt1, pt2):
    # pt1,pt2 = (pt1,pt2) if pt2[0]>pt1[0] else (pt2,pt1)
    try:
        m = (pt2[1] - pt1[1]) / (pt2[0] - pt1[0])
    except ZeroDivisionError:
        return -1, -1

    c = pt1[1] - (m * pt1[0])
    return m, c


def get_bounding_box_lines(geom):
    xmin, ymin, xmax, ymax = geom.bounds
    bp1 = xmin, ymin
    bp2 = xmax, ymin
    bp3 = xmax, ymax
    bp4 = xmin, ymax
    parcleBoundLines = [
        (bp1, bp2),
        (bp2, bp3),
        (bp3, bp4),
        (bp4, bp1),
    ]
    return parcleBoundLines


def get_min_bounding_box_lines(geom):
    g = geom.minimum_rotated_rectangle
    crds = g.exterior.coords[:]
    parcleBoundLines = [(crds[i], crds[i + 1]) for i in range(4)]
    # collection.GeometryCollection([LineString([ps[i],ps[i+1]]) for i in range(4)])
    # bp1,bp2,bp3,bp4 = set(extract_poly_coords()['exterior_coords'])

    # parcleBoundLines = [
    #     (bp1,bp2),
    #     (bp2,bp3),
    #     (bp3,bp4),
    #     (bp4,bp1),
    # ]
    return parcleBoundLines

def get_min_bounding_box_linesegs(geom):
    g = geom.minimum_rotated_rectangle
    crds = g.exterior.coords[:]
    parcleBoundLines = [LineString([crds[i], crds[i + 1]]) for i in range(4)]
    return parcleBoundLines


def get_intersecting_pt(pt1, pt2, p2, m2, c2, parcl_left_crnr, parcl_r8_crnr):
    # y = mx+c, x= (y-c)/m
    # m1*x+c1 = m2*x+c2
    #    x = (c2-c1)/(m1-m2)
    #    y = m2*x+c2
    m1, c1 = get_m_c_of_line(pt1, pt2)
    # is_pt_on_line = lambda yt,xp,m,c: str(m*xp+c)[:4] == str(yt)[:4]
    is_pt_on_line = lambda x, y: (x <= parcl_r8_crnr[0] and x >= parcl_left_crnr[0]) and (
    (y <= parcl_r8_crnr[1] and y >= parcl_left_crnr[1]))
    # is_pt_on_line = lambda x,y,pt1,pt2 : (x<=pt2[0] and x>=pt1[0]) and ((y<=pt2[1] and y>=pt1[1]))
    # ln = LineString([Point(pt1),Point(pt2)])
    # if both horizontal lines or parallel lines
    if (m1 == -1 and m2 == -1) or (m1 == m2):
        return False, -1, -1
    # if both vertical lines
    elif m1 == 0 and m2 == 0:
        return False, -1, -1
    # if m2 horizontal and m1 vertical
    if (m1 == -1 and m2 == 0):
        x = pt1[0]
        y = c2
        return True, x, y
    # if m2 vertical and m1 horizontal
    if (m2 == -1 and m1 == 0):
        x = p2[0]
        y = c1
        return True, x, y

    if m2 != -1 and c2 != -1:
        if m1 != -1 and c1 != -1:
            x = (c2 - c1) / (m1 - m2)
            y = m2 * x + c2
            pt_on_line = is_pt_on_line(x, y)  # Point(x,y).within(ln)
        else:
            x = pt1[0]
            y = m2 * x + c2
            ymn, ymx = min(pt1[1], pt2[1]), max(pt1[1], pt2[1])
            pt_on_line = y < ymx and y > ymn
        return pt_on_line, x, y
    else:
        if m1 != -1 and c1 != -1:
            x = p2[0]
            y = m1 * x + c1
            pt_on_line = is_pt_on_line(x, y)
            return pt_on_line, x, y
        else:
            return False, -1, -1


def group_parllel_lines(df, thresh_degree=10):
    n_rws = df.shape[0]
    angle_dct = {0: [df.iloc[0].geometry]}
    for i in range(1, n_rws):
        l1 = df.iloc[i].geometry
        fnd = False
        for ky, val in angle_dct.items():
            l2 = val[-1]
            angle = abs(angle_between_lines_new(l1, l2))
            if angle < thresh_degree or angle > (180 - thresh_degree):
                fnd = True
                grp_gms = val + [l1]
                break
        if fnd:
            angle_dct[ky] = grp_gms
        else:
            angle_dct[len(angle_dct)] = [l1]

    return angle_dct


def remove_nearest_lines(df, min_length):
    n_rws = df.shape[0]
    angle_dct = {0: [df.iloc[0].geometry]}
    for i in range(1, n_rws):
        l1 = df.iloc[i].geometry
        c1 = l1.centroid
        fnd = False
        for ky, val in angle_dct.items():
            l2 = val[-1]
            c2 = l2.centroid
            if c1.distance(c2) < min_length:
                fnd = True
                grp_gms = val + [l1]
                break
        if fnd:
            angle_dct[ky] = grp_gms
        else:
            angle_dct[len(angle_dct)] = [l1]

    for ky, val in angle_dct.items():
        ind = np.argmax([i.length for i in val])
        angle_dct[ky] = val[ind]

    return list(angle_dct.values())


def remove_parllel_lines(df, min_len):
    lns = []
    for _, vl in group_parllel_lines(df).items():
        parllel_roads_df = pd.DataFrame({'geometry': vl})
        t = remove_nearest_lines(parllel_roads_df, min_len)
        lns.extend(t)
    return lns


def get_road_geom(roadDf, parcel_geom, minParcelLength,front_edge_path=None):
    # 2. Select road with max length, Take extreme points of road to form straight line
    #   a. If got one line take as it is
    #   b. If got more lines, 
    #       fltr by - 
    #           a. consider taking lines length > min width of parcel.min_rotated_bounding_bx
    #           b. remove parallel lines and consider biggest among the parallel lines
    #           c. after a, if any road lies inside x% remove those
    #       i.  If after filtering one line remained take as it is
    #       ii. If more than one line remained, it might corner property, ignore
    #       iii. If two roads don't intersect might be parallel roads

    if not roadDf.shape[0]:
        road_geom = 0
        # print("No road lines present")
    elif roadDf.shape[0] == 1:
        road_geom = roadDf.iloc[0].copy().geometry
    else:
        if exists(front_edge_path):
            front_edge_df = gpd.read_file(front_edge_path).to_crs(epsg=6933)
            if front_edge_df.shape[0] == 1:
                road_geom = front_edge_df.geometry.values[0]
            else:
                road_geom = front_edge_df.iloc[front_edge_df.length.argmax()].copy().geometry
        else:
            road_geom = 1
    # else:
    #     # fltr by -
    #     # a. consider taking lines length > min width of parcel.min_rotated_bounding_bx
    #     road_df_fltrd_by_length = roadDf[roadDf.length > minParcelLength]
    #     if len(road_df_fltrd_by_length) == 0:
    #         road_df_fltrd_by_length = roadDf.copy()
    #         # b. remove parallel lines and consider faraway than min length of parcel among the parallel lines
    #     if road_df_fltrd_by_length.shape[0] > 1:
    #         roads_fltrd_by_prllel_lines = remove_parllel_lines(road_df_fltrd_by_length, minParcelLength)
    #         if len(roads_fltrd_by_prllel_lines) > 1:
    #             # c. after a, if any road lies inside parcel, remove those
    #             roads_fltrd_inside_parcel = [ln for ln in roads_fltrd_by_prllel_lines if
    #                                          not parcel_geom.buffer(1).contains(ln)]
    #             if len(roads_fltrd_inside_parcel) == 1:
    #                 road_geom = roads_fltrd_inside_parcel[0]
    #             # elif len(roads_fltrd_inside_parcel) == 2:
    #             #     angle_btwn_roads = abs(
    #             #         angle_between_lines_new(roads_fltrd_inside_parcel[0], roads_fltrd_inside_parcel[1]))
    #             #     if angle_btwn_roads > 30:
    #             #         ln1, ln2 = [ln.length for ln in roads_fltrd_inside_parcel]
    #             #         # roads of corner property, assuming back road is 1.5 times of front road 
    #             #         if max(ln1, ln2) / min(ln1, ln2) >= 1.5:
    #             #             road_geom = roads_fltrd_inside_parcel[np.argmin([ln1, ln2])]
    #             #         else:
    #             #             # print("Ignoring a possible corner property")
    #             #             road_geom = None
    #             #     else:
    #             #         # print("Ignoring a possible two side road property")
    #             #         road_geom = None
    #             else:
    #                 # print("Ignoring as more than 1 roads found")
    #                 road_geom = 2
    #         elif len(roads_fltrd_by_prllel_lines) == 1:
    #             road_geom = roads_fltrd_by_prllel_lines[0]
    #         else:
    #             # print("Failed to split")
    #             road_geom = 3
    #     elif len(road_df_fltrd_by_length) == 1:
    #         road_geom = road_df_fltrd_by_length.geometry.values[0]
    #     else:
    #         road_geom = 4
    return road_geom

def assess_back_foliage(geom, min_parcel_length, back_parcel_geom, thresh=10):
    try:
        near_parcel_boundry = geom.buffer(1).intersects(back_parcel_geom)
        minFlgWidth = min([LineString(ln).length for ln in get_min_bounding_box_lines(geom)])
        min_width = (minFlgWidth / min_parcel_length) > (thresh / 100)
        return near_parcel_boundry and min_width
    except Exception as e:
        print(f"Exception {str(e)}, in filtering back foliage")
        return True


# Steps for standard property
# 1. Get max area building of all buildings in parcel
def saveGeoDf(df, save_path, crs=4326):
    df = df.to_crs(crs).copy()
    df.to_file(save_path)


def getExtendedLine(ln,EXTRAPOL_RATIO = 0.5):
    'Creates a line extrapoled in both directions direction'
    p1,p2 = ln.coords[:]
    # EXTRAPOL_RATIO = 10
    # a = p1
    xd,yd = p2[0]-p1[0],p2[1]-p1[1]
    a = (p1[0]-(EXTRAPOL_RATIO*xd), p1[1]-(EXTRAPOL_RATIO*yd))
    b = (p2[0]+(EXTRAPOL_RATIO*xd), p2[1]+(EXTRAPOL_RATIO*yd))

    return LineString([a,b])

def getExtendedLine2(ln,EXTRAPOL_RATIO = 0.5):
    'Creates a line extrapoled in both directions direction'
    c = ln.coords[:]
    p1,p2 = c[0],c[-1]
    # EXTRAPOL_RATIO = 10
    # a = p1
    xd,yd = p2[0]-p1[0],p2[1]-p1[1]
    a = (p1[0]-(EXTRAPOL_RATIO*xd), p1[1]-(EXTRAPOL_RATIO*yd))
    b = (p2[0]+(EXTRAPOL_RATIO*xd), p2[1]+(EXTRAPOL_RATIO*yd))

    return LineString([a,b])

def split_front_back(parcelPth, roadPth, featuresPth, foliagePth,front_edge_path=None,front_foliage_dir ='', back_foliage_dir='', front_back_aoi_dir='',
                             label_col='DN', buildng_ftr=2,corner_property_flag=0,foliage_type='box'):
                            
    """
    Inputs:
        parcelPth : path to raw parcel
        roadPth : path to ossm road lines shape file
        featuresPth : path to Lawn, Bed, Building polygons shape file (LBBD Shape file)
        front_edge_path : path to front edge of the parcel from 
        front_back_aoi_dir : path of folder to save splitted aoi
        label_col : col in LBBD shape to indicate feature
        buildng_ftr : Value of DN to indicate building features in LBBD shape file
        corner_property_flag : Flage from extend parcel to indicate whether it's a corener property or standard property
        foliage_type : folage type "box" or "seg" (segementation polygons)
    Returns:
        
    """
    if corner_property_flag == 0 or corner_property_flag == 3:
        prclDf = gpd.read_file(parcelPth).to_crs(epsg=6933)
        roadDf = gpd.read_file(roadPth).to_crs(epsg=6933)
        ftrsDf = gpd.read_file(featuresPth).to_crs(epsg=6933)
        if foliagePth:
            foliageDf = gpd.read_file(foliagePth)
            foliageDf = foliageDf.to_crs(epsg=6933).copy()
        else: foliageDf = None
        ftrsDf[label_col] = ftrsDf[label_col].astype(type(buildng_ftr))

        bldngDf = ftrsDf[ftrsDf[label_col] == buildng_ftr]
        if not bldngDf.shape[0]: return -1,-1
        maxBldngGeom = bldngDf.iloc[bldngDf.area.argmax()].copy().geometry
        maxBldngGeomBffrd = maxBldngGeom.buffer(0.5)
        parcel_geom = prclDf.geometry.values[0]
        # parcleBoundLines = get_bounding_box_lines(parcel_geom)  #.buffer(20,join_style=2)
        # parcl_bounds = parcel_geom.buffer(1).bounds
        # parcl_left_crnr, parcl_r8_crnr = parcl_bounds[:2], parcl_bounds[2:]
        minParcelLength = min([LineString(i).length for i in
                            get_min_bounding_box_lines(parcel_geom)])  # get_min_bounding_box_lines(parcel_geom))
        road_geom = get_road_geom(roadDf, parcel_geom, minParcelLength,front_edge_path=front_edge_path)

        if not isinstance(road_geom,int):
            # try:
                # road_geom = roadDf.iloc[roadDf.length.argmax()].copy().geometry
                rd_p1, rd_p2 = road_geom.coords[0], road_geom.coords[-1]
                roadLine = LineString([rd_p1, rd_p2])
                # rd_ln_m, rd_ln_c = get_m_c_of_line(rd_p1, rd_p2)
                # 3. Get point on building which is at max dist from line 2
                bldngPtsDct = extract_poly_coords(maxBldngGeomBffrd)
                bldngPts = getFlattedListCoords(bldngPtsDct['exterior_coords'])
                dstncs = [Point(pt).distance(roadLine) for pt in bldngPts]
                # mxDstPt = bldngPts[np.argmax(dstncs)]
                mxDst = max(dstncs)
                mnDst = min(dstncs)
                # 4. Draw a parallel line to line 2 and pt 3
                back_edge,_ = getBackEdge(roadLine.centroid,parcel_geom)
                direction = get_direction(roadLine,back_edge)
                seprtng_line_back = roadLine.parallel_offset(mxDst,direction)
                seprtng_line_front = roadLine.parallel_offset(mnDst,direction)

                for i in range(5):
                    seprtng_line_back = getExtendedLine(seprtng_line_back,2)
                    seprtng_line_front = getExtendedLine(seprtng_line_front,2)
                tmp = seprtng_line_back.intersection(parcel_geom.buffer(10))
                try:
                    if tmp.coords[:]:
                        seprtng_line_df = gpd.GeoDataFrame({'geometry': [seprtng_line_back.intersection(parcel_geom.buffer(10))]}, crs=prclDf.crs)
                        saveGeoDf(seprtng_line_df, join(front_back_aoi_dir, basename(parcelPth).replace(".shp", "_serperating_line.shp")))
                except Exception as e: 
                    print(e)
                    seprtng_line_back = None
                tmp = seprtng_line_front.intersection(parcel_geom.buffer(10))
                try:#if not type(tmp) == "GeometryCollection":  
                    if tmp.coords[:]:
                        seprtng_line_front_df = gpd.GeoDataFrame({'geometry': [seprtng_line_front.intersection(parcel_geom.buffer(10))]}, crs=prclDf.crs)
                        saveGeoDf(seprtng_line_front_df, join(front_back_aoi_dir, basename(parcelPth).replace(".shp", "_serperating_line_front.shp")))
                except Exception as e: 
                    print(e) 
                    seprtng_line_front = None
                
                for i,seprtng_line in enumerate([seprtng_line_back,seprtng_line_front]):
                    parcl_splt_lst = []
                    if seprtng_line is not None:
                        coll = shapely.ops.split(parcel_geom, seprtng_line)
                        parcl_splt_lst = list(coll.geoms)
                    # len should be 2, else there might be error in splitting
                    if len(parcl_splt_lst) == 2:
                        bldng_centroid = maxBldngGeom.centroid
                        frntPrcl, backPrcl = parcl_splt_lst if parcl_splt_lst[0].contains(bldng_centroid) else parcl_splt_lst[::-1]
                        aoi_df = gpd.GeoDataFrame({'geometry': [backPrcl]}, crs=prclDf.crs)
                        if foliagePth:
                            foliage_df = foliageDf.copy()
                            if foliage_type == 'box':
                                foliage_split = foliage_df[foliage_df.geometry.apply(lambda x: backPrcl.contains(x.centroid))].copy()
                                if foliage_split.shape[0]:
                                    foliage_split = gpd.clip(foliage_split, aoi_df)
                            else:
                                foliage_split = gpd.clip(foliage_df, aoi_df)
                                
                        if front_back_aoi_dir:
                            aoi_nm = join(front_back_aoi_dir, basename(parcelPth))
                            if i==1:
                                saveGeoDf(aoi_df, aoi_nm.replace(".shp", "_front.shp"))
                                if foliagePth and front_foliage_dir and foliage_split.shape[0]:
                                    saveGeoDf(foliage_split, join(front_foliage_dir, basename(parcelPth)))
                            if i==0:
                                saveGeoDf(aoi_df, aoi_nm.replace(".shp", "_back.shp"))
                                # foliage_back = foliage_split[
                                #                 foliage_split.geometry.apply(lambda geom: assess_back_foliage(geom, minParcelLength, backPrcl))]

                                if foliagePth and back_foliage_dir and foliage_split.shape[0]:
                                    foliage_back = foliage_split.copy()
                                    saveGeoDf(foliage_back, join(back_foliage_dir, basename(parcelPth)))
            # except Exception as e:
            #     print(e,featuresPth,foliagePth)
    else:
        #.to_crs(epsg=6933)
        if foliagePth and front_foliage_dir:
            foliageDf = gpd.read_file(foliagePth)
            if foliageDf.shape[0]:
                saveGeoDf(foliageDf, join(front_foliage_dir, basename(parcelPth)))

        if front_back_aoi_dir:
            aoi_nm = join(front_back_aoi_dir, basename(parcelPth))
            tmp = gpd.read_file(parcelPth)
            tmp.to_file(aoi_nm.replace(".shp", "_front.shp"))



def get_direction(seprtng_line,back_edge):
    ct = back_edge.centroid
    d1 = ct.distance(seprtng_line.centroid)
    l1 = seprtng_line.parallel_offset(5,"left")
    # l2 = seprtng_line.parallel_offset(5,"right")
    return "left" if ct.distance(l1.centroid)<d1 else "right"

def getBackEdge(refPoint,parcel_geom):
    try:
        b = parcel_geom.boundary.coords
    except NotImplementedError:
        b = extract_poly_coords(parcel_geom)['exterior_coords']
    aoi_edges = [LineString(b[k:k+2]) for k in range(len(b) - 1)]
    aoi_edges_cts = [edge.centroid for edge in aoi_edges]
    dstncs = [refPoint.distance(pt) for pt in aoi_edges_cts]
    max_dst_ind = np.argmax(dstncs)
    back_edge,back_edge_dst = aoi_edges[max_dst_ind],dstncs[max_dst_ind]
    return back_edge,back_edge_dst
    

# CUTTING LAWN WITH LINE
# lawn_df = ftrsDf[ftrsDf[label_col] == lawn_ftr].copy()
# ftrsDf.drop(ftrsDf[ftrsDf[label_col] == lawn_ftr].index, axis=0, inplace=True)
# foliage_polys = foliage_back.geometry.values
# tmpLwnDf = lawn_df.copy()
# clipped = False
# for poly in foliage_polys:
#     if poly.area > foliage_min_area:
#         clipped = True
#         tmpLwnDf = tmpLwnDf.difference(poly.buffer(foliage_shrink_buffer_m))

# if clipped:
#     tmpLwnDf = gpd.GeoDataFrame({'geometry': tmpLwnDf, 'DN': lawn_ftr})

# lawn_modified_df = pd.concat([ftrsDf, tmpLwnDf], axis=0, ignore_index=True)

# get_c = lambda m, x, y: y - (m * x)  # y = mx + c
# parll_c = get_c(rd_ln_m, mxDstPt[0], mxDstPt[1])
# 5. Cut the parcel and choose the backyard polygon -> front and back poly seperated

# int_pts = []
# for p1, p2 in parcleBoundLines:
#     intrscted, x, y = get_intersecting_pt(p1, p2, mxDstPt, rd_ln_m, parll_c, parcl_left_crnr, parcl_r8_crnr)
#     if intrscted and x != -1 and y != -1:
#         nwPt = Point(x, y)
#         near_point = False
#         for old_pt in int_pts:
#             if old_pt.distance(nwPt) < minParcelLength * 0.3:
#                 near_point = True
#                 break
#         if not near_point: int_pts.append(Point(x, y))
# if len(int_pts) == 2:
#     seprtng_line = LineString(int_pts)