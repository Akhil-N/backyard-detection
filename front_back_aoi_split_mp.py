import os,pandas as pd
from glob import glob
from tqdm import tqdm
from os.path import *
# from raster2vector import *
from backYardFns import *
import multiprocessing as mp

if __name__ == '__main__':
    base_Dir = "/media/pictor/56a6e3c2-3e26-44f8-986c-11c15e9e773b/AKhil/Res_pod/ObjectDetection/Data/OD1"
    stts_df = pd.read_csv(
        f"{base_Dir}/front_yard_data/extended_parcels/corner_flags.csv")
    print(stts_df.shape[0])
    print(stts_df.corner_flag.value_counts())
    # out_dir = "/media/pictor/56a6e3c2-3e26-44f8-986c-11c15e9e773b/AKhil/Res_pod/Data/ML_QC_GroundUp_data/data_dump_14thDec/geojsons_1/aseg-format-data/Bed_Rings"
    aois_dir = f"/media/pictor/56a6e3c2-3e26-44f8-986c-11c15e9e773b/AKhil/Res_pod/ObjectDetection/Data_reviewed/OD1/aois_clipped"
    tasks = []
    extend_parcels_dir = f"{base_Dir}/front_yard_data/extended_parcels"
    foliage_type = 'box'
    for n, rw in tqdm(stts_df.iterrows(), total=stts_df.shape[0]):
        shp = rw.aoi
        idNm = basename(shp).split('.')[0]
        # pattern = shp.split('/')[-2]
        front_edge_path = join(extend_parcels_dir, idNm,
                               "temp_files/front_edges.shp")

        aoiPath = join(aois_dir, basename(shp))
        # roads_dir = join("/media/pictor/56a6e3c2-3e26-44f8-986c-11c15e9e773b/AKhil/Res_pod/Data/Foliage/FoliagePatternWiseTestResults/V1/osm_roads",pattern)
        # predTifDir = f"/media/pictor/56a6e3c2-3e26-44f8-986c-11c15e9e773b/AKhil/Res_pod/Data/ML_QC_GroundUp_data/data_dump_14thDec/geojsons_1/aseg-format-data/FoliageResuls/epoch_350"
        foliagePredDir = f"/media/pictor/56a6e3c2-3e26-44f8-986c-11c15e9e773b/AKhil/Res_pod/ObjectDetection/Data_reviewed/OD1/clipped_features_shps"
        road_path = join(extend_parcels_dir, idNm,
                         f"temp_files/roads_{idNm}.shp")
        l_b_bd_vec_path = join(f"{base_Dir}/front_yard_data/epoch_350", f"{idNm}_pred.shp")
        if foliage_type == 'box':
            foliage_vec_path = join(foliagePredDir, f"{idNm}.shp")
        else:
            foliage_vec_path = join(foliagePredDir, f"{idNm}_pred.shp")

        # Output
        res_dir = f"/media/pictor/56a6e3c2-3e26-44f8-986c-11c15e9e773b/AKhil/Res_pod/ObjectDetection/Data_reviewed/OD1/front_yard_data/front_back_splitted"
        back_foliage_dir = join(res_dir, 'back_foliage')
        front_foliage_dir = join(res_dir, 'front_foliage')
        front_back_aoi_dir = join(res_dir, 'front_back_parcels')

        os.makedirs(back_foliage_dir, exist_ok=True)
        os.makedirs(front_foliage_dir, exist_ok=True)
        os.makedirs(front_back_aoi_dir, exist_ok=True)

        if all([exists(foliage_vec_path), exists(l_b_bd_vec_path), exists(road_path), exists(aoiPath)]):
            # split_front_back(parcelPth, roadPth, featuresPth, front_edge_path=None, front_back_aoi_dir='',
            #                  label_col='DN', buildng_ftr=2,corner_property_flag=0)
            tasks.append([aoiPath, road_path, l_b_bd_vec_path, foliage_vec_path, front_edge_path,
                          front_foliage_dir, back_foliage_dir, front_back_aoi_dir,
                          'DN', 3, rw.corner_flag, "box",None])
    print(len(tasks))

    p = mp.Pool(16)
    if len(tasks) == 0:
        print(foliage_vec_path,l_b_bd_vec_path,road_path,aoiPath)
    res = p.starmap_async(split_front_back, tasks)
    p.close()
    p.join()
    r = res.get()