import geopandas as gpd
from backYardFns import *
from shapely.geometry import MultiPolygon

pattern = 'pattern_10'
id = '714'
# output_dir = join(out_dir,pattern)
# os.makedirs(output_dir,exist_ok=True)
back_foliage = f"/media/pictor/56a6e3c2-3e26-44f8-986c-11c15e9e773b/AKhil/Res_pod/Data/Foliage/FoliagePatternWiseTestResults/V1/back_foliage/{pattern}/{id}.shp"
back_foliage_df = gpd.read_file(back_foliage).to_crs(epsg=6933)
back_aoi = gpd.read_file(f"/media/pictor/56a6e3c2-3e26-44f8-986c-11c15e9e773b/AKhil/Res_pod/Data/Foliage/FoliagePatternWiseTestResults/V1/front_back_parcels/{pattern}/{id}_back.shp").to_crs(epsg=6933)
aoi = gpd.read_file(f"/media/pictor/56a6e3c2-3e26-44f8-986c-11c15e9e773b/AKhil/Res_pod/PatternWiseTestData/data_for_e2e_tesing/aois_clipped/{pattern}/{id}.shp").to_crs(epsg=6933)
seprtng_line_df = gpd.read_file(f"/media/pictor/56a6e3c2-3e26-44f8-986c-11c15e9e773b/AKhil/Res_pod/Data/Foliage/FoliagePatternWiseTestResults/V1/front_back_parcels/{pattern}/{id}_serperating_line.shp").to_crs(epsg=6933)


# 1. Consider largest back foliage
seprtng_line = seprtng_line_df.geometry.values[0]
back_aoi_geom = back_aoi.geometry.values[0]
aoi_geom = aoi.geometry.values[0]
try:
    b = back_aoi_geom.boundary.coords
except NotImplementedError:
    b = extract_poly_coords(back_aoi_geom)['exterior_coords']
back_aoi_edges = [LineString(b[k:k+2]) for k in range(len(b) - 1)]
seprtng_line_ct = seprtng_line.intersection(aoi_geom.buffer(5)).centroid #seprtng_line.centroid

same_edge = np.argmin([seprtng_line_ct.distance(edge) for edge in back_aoi_edges])
indcs = np.where(np.array([abs(angle_between_lines_new(seprtng_line,l)) for l in back_aoi_edges])<30)[0]
indcs = np.delete(indcs,np.where(indcs==same_edge))
aoi_edges_cts = [edge.centroid for edge in back_aoi_edges]
dstncs = [seprtng_line_ct.distance(pt) for pt in aoi_edges_cts]
max_dst_ind = np.argmax(dstncs)
back_edge_dst = dstncs[max_dst_ind]

if len(indcs)==1:
    back_edge = back_aoi_edges[indcs[0]]
    back_edge = getExtendedLine(back_edge,5)
    ref = 'back'
else:
    back_edge = back_aoi_edges[max_dst_ind]
    ref = 'front'

back_foliage = back_foliage_df.explode()
back_foliage_tchng_back = back_foliage[back_foliage.geometry.apply(lambda x: x.buffer(1).intersects(back_edge))]
if back_foliage_tchng_back.shape[0]:
    maxFlgGeom = MultiPolygon(back_foliage_tchng_back.geometry.values)
# else:
#     return None
minAoiBxLen = min([LineString(i).length for i in
                        get_min_bounding_box_lines(back_aoi_geom)])
maxFlgBxLen = max([LineString(i).length for i in
                        get_min_bounding_box_lines(maxFlgGeom)])

# back_aoi_boundry = back_aoi_geom.boundary #LineString(extract_poly_coords(back_aoi_geom)['exterior_coords'])
#     a. Largest min bx len of Foliage > 90% min of min bx len of aoi
#     b. Foliage touches back aoi
maxFlgGeomBuff = maxFlgGeom.buffer(1)
# Getting Back edge of parcels

if ref == 'front':
    split_line = seprtng_line
    split_line = getExtendedLine(split_line,3)
else:
    dir_tmp = get_direction(back_edge,seprtng_line)
    dst = back_edge.centroid.distance(seprtng_line_ct)
    split_line = back_edge.parallel_offset(dst,dir_tmp)

direction = get_direction(split_line,back_edge)
###############################
if maxFlgBxLen > minAoiBxLen*0.8 and maxFlgGeomBuff.intersects(back_edge) :
# 2. Find the nearest foliage pt from seperating line
    i,n_splits = 1,15
    mn_dst = 0
    if not maxFlgGeomBuff.intersects(seprtng_line):
        flgPtsDct = extract_poly_coords(maxFlgGeom)
        flgPts = getFlattedListCoords(flgPtsDct['exterior_coords'])
        dsts = [Point(pt).distance(split_line) for pt in flgPts]
        mn_ind = np.argmin(dsts)
        mn_dst = dsts[mn_ind]
        minDstPt = flgPts[mn_ind]
        back_edge_dst = Point(minDstPt).distance(back_edge.centroid)
        
    back = back_aoi_geom
    back_flg = maxFlgGeom
    ratios,area_ratios,back_splt_lns=[],[],[]
    while i<n_splits:
    # 3. Draw a parallel line lp from pt 2 to seperating line
        if mn_dst==0 or i>1: mn_dst += (back_edge_dst/n_splits)
        back_ln = split_line.parallel_offset(mn_dst,direction)
        coll = shapely.ops.split(back, back_ln)
        parcl_splt_lst = list(coll.geoms)
        # len should be 2, else there might be error in splitting
        if len(parcl_splt_lst) == 2:
            ct1,ct2 = parcl_splt_lst[0].centroid,parcl_splt_lst[1].centroid
            d1,d2 = ct1.distance(split_line),ct2.distance(split_line)
            back,othr = (parcl_splt_lst[0],parcl_splt_lst[1]) if d1>d2 else (parcl_splt_lst[1],parcl_splt_lst[0])

            coll = shapely.ops.split(back_flg, back_ln)
            flg_splt_lst = list(coll.geoms)
            # if len(flg_splt_lst) ==2:
            #     f_ct1 = flg_splt_lst[0].centroid
            #     back_flg,frnt_flg = (flg_splt_lst[0],flg_splt_lst[1]) if back.contains(f_ct1) else (flg_splt_lst[1],flg_splt_lst[0])
            if len(flg_splt_lst) ==1:
                back_flg = flg_splt_lst[0]

            if len(flg_splt_lst)>1:
                f_cts = [f.centroid for f in flg_splt_lst]
                back_flgs = [f for f,ct in zip(flg_splt_lst,f_cts) if back.contains(ct)]
                back_flg = MultiPolygon(back_flgs)
            
            r = back_flg.area/back.area
            area_r = back.area/back_aoi_geom.area
            if r<0.97 and  area_r > 0.01:
                ratios.append(r)
                back_splt_lns.append(back_ln)
                area_ratios.append(area_r)
            print(back.area/back_aoi_geom.area,r)

            if r>= 0.97 and r<1 and  area_r > 0.01:
                back_ln = gpd.GeoDataFrame({'geometry': [back_ln.intersection(aoi_geom.buffer(5))]}, crs=aoi.crs)
                break
            i+=1
            # if i==2:break

        else:
            print("Not able to split")
            break
    # if ratios:
    #     back_ln =  pick_final_ln(ratios = ratios,area_ratios= area_ratios,split_lines= back_splt_lns,aoi= aoi_geom,crs = aoi.crs,min_thresh=90,min_area = 1)